//
//  EditProfilePresenter.swift
//  AppRealSample
//
//  Created by Artem Shvets on 25.01.2020.
//  Copyright © 2020 ArtemShvets. All rights reserved.
//

import UIKit

class EditProfilePresenter: EditProfilePresenterProtocol {

    var view: EditProfileViewControllerProtocol!
    
    func loadData() {
        
    }
    
    func save(name: String, for person: PersonStruct) {
        
        if name != person.name {
            var person = person
            person.name = name
            PersonsSDK.shared.savePerson(person: person, for: view.person.id)
        }
    }
}
