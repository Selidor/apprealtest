//
//  EditProfileViewController.swift
//  AppRealSample
//
//  Created by Artem Shvets on 25.01.2020.
//  Copyright © 2020 ArtemShvets. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController, EditProfileViewControllerProtocol {
    
    @IBOutlet weak var textField: UITextField!
    
    var person: PersonStruct!
    var presenter: EditProfilePresenterProtocol!
    
    private let coordinator: EditProfileCoordinator = EditProfileCoordinator()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        coordinator.configureModule(viewController: self)
        self.textField.text = person.name
    }

    @IBAction func doneButtonAction(_ sender: Any) {
        if let name = textField.text {
            presenter.save(name: name, for: person)
            self.navigationController?.popViewController(animated: true)
        }
    }
}
