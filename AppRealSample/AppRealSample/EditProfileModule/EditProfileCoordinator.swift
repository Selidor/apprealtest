//
//  EditProfileCoordinator.swift
//  AppRealSample
//
//  Created by Artem Shvets on 25.01.2020.
//  Copyright © 2020 ArtemShvets. All rights reserved.
//

import UIKit

class EditProfileCoordinator: EditProfileCoordinatorProtocol {
    
    func configureModule(viewController: EditProfileViewControllerProtocol) {
        
        let presenter = EditProfilePresenter()
        presenter.view = viewController
        viewController.presenter = presenter
    }
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
}
