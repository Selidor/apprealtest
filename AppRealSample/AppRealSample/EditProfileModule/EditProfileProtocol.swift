//
//  EditProfileProtocol.swift
//  AppRealSample
//
//  Created by Artem Shvets on 25.01.2020.
//  Copyright © 2020 ArtemShvets. All rights reserved.
//

import UIKit

protocol EditProfileViewControllerProtocol: class {
    
    var presenter: EditProfilePresenterProtocol! {set get}
    var person: PersonStruct! {set get}
}

protocol EditProfilePresenterProtocol: class {
    
    var view: EditProfileViewControllerProtocol! {set get}
    func loadData()
    func save(name: String, for person: PersonStruct)
    
}

protocol EditProfileCoordinatorProtocol: class {
    
    func configureModule(viewController: EditProfileViewControllerProtocol)
    func prepare(for segue: UIStoryboardSegue, sender: Any?)
}
