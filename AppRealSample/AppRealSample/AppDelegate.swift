//
//  AppDelegate.swift
//  AppRealSample
//
//  Created by Artem Shvets on 24.01.2020.
//  Copyright © 2020 ArtemShvets. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        let coreDataStore = CoreDataStore.shared
        coreDataStore.saveContext()
    }

}

