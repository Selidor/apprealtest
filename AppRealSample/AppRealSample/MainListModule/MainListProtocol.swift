//
//  MainListProtocol.swift
//  AppRealSample
//
//  Created by Artem Shvets on 25.01.2020.
//  Copyright © 2020 ArtemShvets. All rights reserved.
//

import UIKit

typealias downloadImageCompletionHandler = (UIImage) -> Void
typealias queryCompletionWithErrorMessage = (String) -> Void

protocol MainListViewControllerProtocol: class {
    
    var presenter: MainListPresenterProtocol! {set get}
    var selectedPerson: PersonStruct? {set get}
    
    func didLoadData()
    func configureCell(at indexPath: IndexPath)
    
}

protocol MainListPresenterProtocol: class {
    
    var view: MainListViewControllerProtocol! {set get}
    var personList: [PersonStruct]? { set get }
    
    func loadData()
    func getImageAsync(from name: String, success: @escaping downloadImageCompletionHandler, failure: @escaping queryCompletionWithErrorMessage)
    
}

protocol MainListCoordinatorProtocol: class {
    
    func configureModule(viewController: MainListViewControllerProtocol)
    func prepare(for segue: UIStoryboardSegue, sender: Any?)
}
