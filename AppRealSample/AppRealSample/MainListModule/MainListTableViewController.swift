//
//  MainListTableViewController.swift
//  AppRealSample
//
//  Created by Artem Shvets on 25.01.2020.
//  Copyright © 2020 ArtemShvets. All rights reserved.
//

import UIKit
import Foundation


class MainListTableViewController: UITableViewController, MainListViewControllerProtocol {


    private let coordinator: MainListCoordinator = MainListCoordinator()
    
    var presenter: MainListPresenterProtocol!
    var selectedPerson: PersonStruct?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        coordinator.configureModule(viewController: self)
        tableView.tableFooterView = UIView()
        
        if let presenter = presenter {
            
            refreshControl?.beginRefreshing()
            presenter.loadData()
        }
    }
    
    func didLoadData() {
        
        refreshControl?.endRefreshing()
        self.tableView.reloadData()
        showCongrats()
    }
    
    
    func configureCell(at indexPath: IndexPath) {
        tableView.reloadData()
    }
    
    private func reuseId() -> String!{
        let reuseId = String(describing: ListCell.self)
        return reuseId
    }
    
    @objc func refresh(){
        presenter.loadData()
    }
    
    func showCongrats(){

        let alertController = UIAlertController(title: "Congratulation!", message:
            "All data has been downloaded, and saved successfully!", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default))

        self.present(alertController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        coordinator.prepare(for: segue, sender: sender)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.personList?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseId(), for: indexPath) as! ListCell
        
        if let person = presenter.personList?[indexPath.row]{
            
            cell.title.text = person.name
            
            if let personImageName =  person.localImageName {
                presenter.getImageAsync(from: personImageName, success: { (image) in
                    DispatchQueue.main.async {
                        cell.avatarImageView.image = image
                    }
                }) { (errorDescription) in
                    
                }
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedPerson = presenter.personList?[indexPath.row]
        performSegue(withIdentifier: "ShowDetail", sender: self)
        tableView.deselectRow(at: indexPath, animated: false)
    }

}
