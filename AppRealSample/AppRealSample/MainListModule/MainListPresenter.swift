//
//  MainListPresenter.swift
//  AppRealSample
//
//  Created by Artem Shvets on 25.01.2020.
//  Copyright © 2020 ArtemShvets. All rights reserved.
//

import UIKit
import CoreData

class MainListPresenter: NSObject, MainListPresenterProtocol, NSFetchedResultsControllerDelegate {
 
    var personList: [PersonStruct]?
    weak var view: MainListViewControllerProtocol!
    private let personSDK = PersonsSDK()
    private var fetchedResultsController: NSFetchedResultsController<Person>!
    
    func loadData() {
        
        fetchedResultsController = CoreDataStore.shared.createFetchedResultsController()
        fetchedResultsController.delegate = self
        
        personSDK.getPersonList(success: { (personList) in
            
            self.personList = personList
            DispatchQueue.main.async {
                self.view.didLoadData()
            }
            
        }) { (errorString) in
            
        }
    }
    
    func getImageAsync(from imageName: String, success: @escaping downloadImageCompletionHandler, failure: @escaping queryCompletionWithErrorMessage) {
        
        var documentPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        documentPath?.append("/\(imageName)")
        let url = URL(fileURLWithPath: documentPath!)
        
        DispatchQueue.global(qos: .background).async {
            do {
                let imageData = try Data(contentsOf: url)
                let image = UIImage(data: imageData)!
                success(image)
            } catch {
                print("Unable to load data: \(error)")
            }
        }
    }
    
    // MARK: - Fetched results controller

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert: break
        case .delete: break
        case .update:
            if let person = PersonMapper.convertFrom(managedObject: anObject as! Person){
                let row = indexPath!.row
                personList?[row] = person
                view.configureCell(at: indexPath!)
            }
        case .move: break
        default:
            return
        }
    }
    

}
