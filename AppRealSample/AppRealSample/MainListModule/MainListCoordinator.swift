//
//  MainListCoordinator.swift
//  AppRealSample
//
//  Created by Artem Shvets on 25.01.2020.
//  Copyright © 2020 ArtemShvets. All rights reserved.
//

import UIKit

class MainListCoordinator: MainListCoordinatorProtocol {
    
    func configureModule(viewController: MainListViewControllerProtocol) {
        
        let presenter = MainListPresenter()
        presenter.view = viewController
        viewController.presenter = presenter
    }
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destinationVC = segue.destination as? EditProfileViewControllerProtocol
        let sourceVC = segue.source as? MainListViewControllerProtocol
        let person = sourceVC?.selectedPerson
        destinationVC?.person = person
    }
    

}
