//
//  PersonMapper.swift
//  AppRealSample
//
//  Created by Artem Shvets on 27.01.2020.
//  Copyright © 2020 ArtemShvets. All rights reserved.
//

import UIKit

class PersonMapper {

    class func convertFrom(managedObject: Person) -> PersonStruct? {
        
        var personStruct = PersonStruct()
        
        personStruct.name = managedObject.name
        personStruct.id = Int(managedObject.id)
        
        if let imageURL = managedObject.imageURL{
            personStruct.imageURL = imageURL
        }
        
        if let localImageName = managedObject.localImageName{
            personStruct.localImageName = localImageName
        }
        
        return personStruct
    }
    
    class func convert(managedObjects: [Person]) -> [PersonStruct]? {
        
        var structs = [PersonStruct]()
        
        for mo in managedObjects {
            
            if let strp = convertFrom(managedObject: mo){
                structs.append(strp)
            }
        }
        
        return structs
    }
    
    
}
