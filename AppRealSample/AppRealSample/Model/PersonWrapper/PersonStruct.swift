//
//  PersonStruct.swift
//  AppRealSample
//
//  Created by Artem Shvets on 27.01.2020.
//  Copyright © 2020 ArtemShvets. All rights reserved.
//

import Foundation

struct PersonStruct {
    var id: Int!
    var name: String!
    var imageURL: URL?
    var localImageName: String?
}
