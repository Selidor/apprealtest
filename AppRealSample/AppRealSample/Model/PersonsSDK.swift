//
//  ProfileSDK.swift
//  AppRealSample
//
//  Created by Artem Shvets on 25.01.2020.
//  Copyright © 2020 ArtemShvets. All rights reserved.
//

import UIKit
import Foundation

extension Int {
    func format(f: String) -> String {
        return String(format: "%\(f)d", self)
    }
}

class PersonsSDK {
    
    typealias queryCompletionHandler = (Array<PersonStruct>) -> Void
    typealias queryCompletionWithErrorMessage = (String) -> Void
        
    private(set) var personList = [PersonStruct]()
    
    private let localDataManager = LocalDataManager()
    private let networkManager = NetworkManager()
    private var personDict: [URL:PersonStruct]?
    
    static var shared: PersonsSDK = {
        let instance = PersonsSDK()
        return instance
    }()
    
    func getPersonList(success: @escaping queryCompletionHandler, failure: @escaping queryCompletionWithErrorMessage){
        
        if let persons = try? localDataManager.getPersons(), persons.count > 0{
            personList = persons
            success(persons)
            return
        }
        
        var links = [URL]()
        personDict = [URL:PersonStruct]()
        
        for (index, element) in self.nameList()!.enumerated() {
            
            guard let link = self.getLinkAt(index: index) else {
                failure("Link Not Exist")
                return
            }
            guard let url = URL.init(string: link) else {
                failure("URL Not Exist")
                return
            }
            
            links.append(url)
            
            var person = PersonStruct()
            person.id = index
            person.imageURL = url
            person.name = element
            
            personDict![url] = person
        }
        
        weak var weakSelf = self
        
        self.networkManager.process(URLs: links, loadImage: { (fromURL, toURL) in
            
            print("did finish download:\(String(describing: toURL.path))")
            
            if var person = weakSelf?.personDict?[fromURL] {
                person.localImageName = toURL.lastPathComponent
                weakSelf?.personList.append(person)
            }
            
        }) {
            
            weakSelf?.personDict?.removeAll()
            if let weakSelf = weakSelf {
                try? weakSelf.localDataManager.saveDownloaded(persons: weakSelf.personList)
            }
            
            success(weakSelf!.personList)
            print("finish")
        }
        
    }
    
    func savePerson(person: PersonStruct, for id: Int){
        
        if let personEnt = try? localDataManager.personAt(id: id){
            
            personEnt.name = person.name
            personEnt.imageURL = person.imageURL
            personEnt.localImageName = person.localImageName
            CoreDataStore.shared.update(object: personEnt)
            
        }else{
            // Create new entity
        }
        
    }
    
    private func getLinkAt(index: Int) -> String! {
        
        let intFormat = "02"
        let query = "http://download.glide.me/pre-login-avatars/avatars_cartoon_animals_\((index + 1).format(f: intFormat)).png"
        return query
    }
    
    private func nameList() -> [String]! {
        return ["Adam", "Henry", "Jeremy", "Kyle", "Mark",
                "Douglas", "Marion", "Jade", "Madison", "Robert",
                "Peyton", "Rodney", "Lucas", "Sam", "Eugene",
                "Laurie", "Jason", "Edward", "Toby", "Johnny"]
    }

}
