//
//  CoreDataStore.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import UIKit
import CoreData

class CoreDataStore: NSObject {
    
    static var shared: CoreDataStore = {
        let instance = CoreDataStore()
        return instance
    }()
    
    private override init() {}
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "AppRealSample")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func update(object: NSManagedObject){
        let context = persistentContainer.viewContext
        context.refresh(object, mergeChanges: true)
        saveContext()
    }
    
    func delete(object: NSManagedObject){
        let context = persistentContainer.viewContext
        context.delete(object)
        saveContext()
    }

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    var persistentStoreCoordinator: NSPersistentStoreCoordinator? {
        return persistentContainer.persistentStoreCoordinator
    }
    
    var managedObjectModel: NSManagedObjectModel? {
        return persistentContainer.managedObjectModel
    }
    
    var managedObjectContext: NSManagedObjectContext? {
        return persistentContainer.viewContext
    }
    
    func createFetchedResultsController() -> NSFetchedResultsController<Person> {
        
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        fetchRequest.fetchBatchSize = 20
        
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: "Main")
        
        do {
            try aFetchedResultsController.performFetch()
        } catch {
             let nserror = error as NSError
             fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return aFetchedResultsController
    }
    
    
}
