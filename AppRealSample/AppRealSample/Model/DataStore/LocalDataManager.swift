//
//  LocalDataManager.swift
//  AppRealSample
//
//  Created by Artem Shvets on 27.01.2020.
//  Copyright © 2020 ArtemShvets. All rights reserved.
//

import CoreData

enum PersistenceError: Error {
    case managedObjectContextNotFound
    case couldNotSaveObject
    case objectNotFound
    case fetchError
}

class LocalDataManager {
    
    func saveDownloaded(persons: [PersonStruct]) throws {
        
        guard let managedOC = CoreDataStore.shared.managedObjectContext else {
            throw PersistenceError.managedObjectContextNotFound
        }
        
        for persStruct in persons {
            
            if let personEntity = NSEntityDescription.entity(forEntityName: String(describing: Person.self), in: managedOC){
                
                if personExists(imageName: persStruct.localImageName!, moc: managedOC){
                    continue
                }
                
                let person = Person(entity: personEntity, insertInto: managedOC)
                person.id = Int16(persStruct.id)
                person.name = persStruct.name
                person.imageURL = persStruct.imageURL
                person.localImageName = persStruct.localImageName
                
            }
        }
        
        CoreDataStore.shared.saveContext()
        
    }
    
    func getPersons() throws -> [PersonStruct]? {
        
        guard let managedOC = CoreDataStore.shared.managedObjectContext else {
            throw PersistenceError.managedObjectContextNotFound
        }
        
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: true)
        
        let request: NSFetchRequest<Person> = NSFetchRequest(entityName: String(describing: Person.self))
        request.sortDescriptors = [sortDescriptor]
        
        var persons: [Person]?
        
        do {
            persons = try managedOC.fetch(request)
        } catch {
            throw PersistenceError.fetchError
        }
        
        if let persons = persons, let personStructs = PersonMapper.convert(managedObjects: persons){
            return personStructs
        }
        
        return nil
    }
    
    func personAt(id: Int) throws -> Person? {
        
        guard let managedOC = CoreDataStore.shared.managedObjectContext else {
            throw PersistenceError.managedObjectContextNotFound
        }
        
        let fetchRequest = NSFetchRequest<Person>(entityName: "Person")
        fetchRequest.includesSubentities = false
        
        var predicate = NSPredicate()
        predicate = NSPredicate(format: "id == %i", id)
        fetchRequest.predicate = predicate
    
        var person: Person?
        
        do {
            let persons = try managedOC.fetch(fetchRequest)
            person = persons.first
        } catch {
            throw PersistenceError.fetchError
        }
        
        return person
    }
    
    func personExists(imageName: String, moc: NSManagedObjectContext) -> Bool {
        
        let fetchRequest = NSFetchRequest<Person>(entityName: "Person")
        fetchRequest.includesSubentities = false
        
        var predicate = NSPredicate()
        predicate = NSPredicate(format: "localImageName == %@", imageName)
        fetchRequest.predicate = predicate
        var entitiesCount = 0
        
        do {
            entitiesCount = try moc.count(for: fetchRequest)
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        
        return entitiesCount > 0
    }
    
}
