//
//  NetworkManager.swift
//  Reddline
//
//  Created by Artem Shvets on 19.11.2019.
//  Copyright © 2019 ArtemShvets. All rights reserved.
//

import Foundation
import UIKit

typealias loadImageBlock = (URL, URL) -> Void
typealias completionLoadBlock = () -> Void

class NetworkManager: NSObject, URLSessionDelegate, URLSessionDownloadDelegate {
    
    private var loadImageHandler: loadImageBlock?
    
    private let semaphore = DispatchSemaphore(value: 0)
    private var processedURL: URL?
    private let imageDownloadingQueue = OperationQueue()
    
    override init() {
        
        imageDownloadingQueue.name = "imageDownloadingQueue"
        super.init()
    }
    
    func process(URLs: [URL]!, loadImage: @escaping loadImageBlock ,completion: @escaping completionLoadBlock){
        
        weak var weakSelf = self
        
        imageDownloadingQueue.addOperation {
            
            self.loadImageHandler = loadImage
            
            let queue = OperationQueue()
            queue.name = "downloadTask"
            queue.maxConcurrentOperationCount = 1
            
            let sessionConfiguration = URLSessionConfiguration.background(withIdentifier: "SessionID")
            let urlSession = URLSession(configuration: sessionConfiguration, delegate: weakSelf , delegateQueue: nil)
            
            for url in URLs {
                
                queue.addOperation {
                    weakSelf?.processedURL = url
                    print("starting download: \(url.path)")
                    let downloadTask = urlSession.downloadTask(with: url)
                    // cause .resume() calls async
                    downloadTask.resume()
                    weakSelf?.semaphore.wait()
                }
            }
            
            queue.waitUntilAllOperationsAreFinished()
            
            weakSelf?.loadImageHandler = nil
            urlSession.finishTasksAndInvalidate()
            completion()
        }
        
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        if let processedURL = processedURL, let savedURL = saveCashedFileAt(url: location, with: processedURL.lastPathComponent){
            loadImageHandler?(processedURL, savedURL)
        }
        
        semaphore.signal()
        
    }
    
    
    private func saveCashedFileAt(url: URL!, with name: String!) -> URL?{
        
        var savedURL: URL?
        let fileManger = FileManager.default
        
        do {
            var doumentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
            
            doumentDirectoryPath = doumentDirectoryPath.appendingPathComponent(name) as NSString
            
            savedURL = URL.init(fileURLWithPath: doumentDirectoryPath as String)
            
            if let savedURL = savedURL, !fileManger.fileExists(atPath: savedURL.path) {
                
                try fileManger.moveItem(at: url, to: savedURL)
                if fileManger.fileExists(atPath: url.path){
                    try fileManger.removeItem(at: url)
                }
            }
            
        } catch {
            savedURL = nil
            print ("file error: \(error)")
        }
        
        return savedURL
    }
}
